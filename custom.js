jQuery(document).ready(function ($) {
   /*$('.ct-right-home-page').each(function () {
       var parent = $(this);

       $('.list_tour', parent).each(function () {
           var me = $(this);
           var link = $('.thumb-header a', me).attr('href');
           $('.thumb-caption', me).append('<a href="'+ link +'" class="btn btn-primary btn-sm mt5 pull-right">'+ $('.thumb-header', me).find('h5.hover-title-center').text() +'</a>');
       });

   });*/

   $('.st-location-name  ').each(function () {
      var t = $(this);
      t.on('focus', function () {
          setTimeout(function () {
              $('.st-option-wrapper .option').each(function () {
                  var me = $(this);
                  var text = me.data('text');
                  var strF = text.indexOf('(');
                  var strL = text.indexOf(')');

                  if(strF > -1){
                      var mainText = text.substring(0, strF);
                      var subText = text.substring(strF, strL + 1);

                      if(subText != ''){
                          subText = "&nbsp;<span class='type'><small>" + subText + "</small></span>";
                      }

                      var text_split = mainText.split("||");
                      text_split = text_split[0];

                      var allText = text_split.trim() + '<i class="fa fa-map-marker"></i> ' + subText;

                  }else{
                      var text_split = text.split("||");
                      text_split = text_split[0];
                      allText = text_split + '<i class="fa fa-map-marker"></i>';
                  }
                  me.html('<span class="label"><a href="#">'+ allText +'</a></span>');
              });
          }, 1)
      });
   });
});