<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 21/08/2015
 * Time: 9:45 SA
 */
function child_theme_slug_setup () {
	load_child_theme_textdomain (ST_TEXTDOMAIN, get_stylesheet_directory (). '/ language');
}
add_action ('after_setup_theme', 'child_theme_slug_setup');

add_action( 'wp_enqueue_scripts', 'ct_add_scripts' );

function ct_add_scripts() {
	wp_enqueue_style( 'ct-custom-css', get_stylesheet_directory_uri() . '/custom.css' );
	wp_enqueue_script( 'ct-custom-js', get_stylesheet_directory_uri() . '/custom.js', [ 'jquery' ], null, true );
}
if ( ! function_exists( 'st_email_booking_infant_info' ) ) {
	function st_email_booking_infant_info( $atts = array() ) {
		global $order_id;
		if ( $order_id ) {
			$post_id   = trim( get_post_meta( $order_id, 'item_id', true ) );
			$post_type = get_post_type( $post_id );
			if ( $post_type == 'st_tours' || $post_type == 'st_activity' || $post_type == "st_hotel" || $post_type == 'hotel_room' ) {
				$data   = shortcode_atts( array(
					'title' => __( 'No. Infant(s)', ST_TEXTDOMAIN ),
				), $atts );
				$infant = intval( get_post_meta( $order_id, 'infant_number', true ) );

				$currency = get_post_meta( $order_id, 'currency', true );
				//$rate = floatval(get_post_meta($order_id,'currency_rate', true));
				$infant_price = floatval( get_post_meta( $order_id, 'infant_price', true ) );

				$data_price = get_post_meta( $order_id, 'data_price', true );
				if ( ! empty( $data_price['infant_price'] ) ) {
					$infant_price = $data_price['infant_price'] / $infant;
				}

				$discount = get_post_meta( $post_id, 'discount', true );
				if ( $discount > 0 ) {
					$infant_price = $infant_price - ( $infant_price * ( $discount / 100 ) );
				}

				$infant_price_html = ' x ' . TravelHelper::format_money_from_db( $infant_price, $currency );
				if ( $post_type == "st_hotel" || $post_type == 'hotel_room' ) {
					$infant_price_html = "";
				}
				if ( $infant > 0 ) {
					$html = '
                    <table width="100%">
                        <tr>
                            <td style="border-bottom: 1px dashed #ccc; text-align: left; padding-left: 20px;">' . $data['title'] . '</td>
                            <td style="text-align: right; border-bottom: 1px dashed #CCC; padding-right: 10px;"><p>' . $infant . ' ' . __( 'infant(s)', ST_TEXTDOMAIN ) . $infant_price_html . '</p></td>
                        </tr>
                    </table>
                    ';

					return $html;
				}
			} else {
				return '';
			}
		}

		return '';
	}
}
st_reg_shortcode( 'st_email_booking_infant_info', 'st_email_booking_infant_info' );

if ( ! function_exists( 'st_email_booking_bank_info' ) ) {
	function st_email_booking_bank_info( $atts = array() ) {
		global $order_id;
		if ( $order_id ) {
			$payment_method1 = get_post_meta( $order_id, 'payment_method', true );
			if ( $payment_method1 == 'st_submit_form' ) {
				echo '<h4>' . __( 'Bank Transfer Account Details', ST_TEXTDOMAIN ) . '</h4>';
				?>
                <div class="pm-info ct-bank-info">
					<?php
					$accounts = st()->get_option( 'submit_form_account' );
					if ( ! empty( $accounts ) ) {
						?>
                        <table class="table-responsive">
                            <tr class="hidden-xs">
                                <!--<th><?php /*echo __( 'Account name', ST_TEXTDOMAIN ); */?></th>-->
                                <th><?php echo __( 'Account number', ST_TEXTDOMAIN ); ?></th>
                                <th><?php echo __( 'Bank name', ST_TEXTDOMAIN ); ?></th>
                                <!--<th><?php /*echo __( 'Sort code', ST_TEXTDOMAIN ); */?></th>
                                <th><?php /*echo __( 'IBAN', ST_TEXTDOMAIN ); */?></th>-->
                                <th><?php echo __( 'BIC / Swift', ST_TEXTDOMAIN ); ?></th>
                            </tr>
							<?php
							foreach ( $accounts as $k => $v ) {
								?>
                                <tr>
                                    <!--<td>
                            <span class="hidden-lg hidden-md hidden-sm"><?php /*echo __( 'Account name', ST_TEXTDOMAIN ); */?>
                                : </span><?php /*echo $v['account_name']; */?></td>-->
                                    <td>
                            <span class="hidden-lg hidden-md hidden-sm"><?php echo __( 'Account number', ST_TEXTDOMAIN ); ?>
                                : </span><?php echo $v['account_number']; ?></td>
                                    <td><span class="hidden-lg hidden-md hidden-sm"><?php echo __( 'Bank name', ST_TEXTDOMAIN ); ?>
                                            : </span><?php echo $v['bank_name']; ?></td>
                                    <!--<td><span class="hidden-lg hidden-md hidden-sm"><?php /*echo __( 'Sort code', ST_TEXTDOMAIN ); */?>
                                            : </span><?php /*echo $v['sort_code']; */?></td>
                                    <td><span class="hidden-lg hidden-md hidden-sm"><?php /*echo __( 'IBAN', ST_TEXTDOMAIN ); */?>
                                            : </span><?php /*echo $v['iban']; */?></td>-->
                                    <td>
                            <span class="hidden-lg hidden-md hidden-sm"><?php echo __( 'BIC / Swift', ST_TEXTDOMAIN ); ?>
                                : </span><?php echo $v['bic']; ?></td>
                                </tr>
								<?php
							}
							?>
                        </table>
						<?php
					}
					?>
                </div>
			<?php }
		}
		return '';
	}
}
st_reg_shortcode( 'st_email_booking_bank_info', 'st_email_booking_bank_info' );