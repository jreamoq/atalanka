<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Hotel field room num
 *
 * Created by ShineTheme
 *
 */
$default=array(
    'title'=>''
);

if(isset($data)){
    extract(wp_parse_args($data,$default));
}else{
    extract($default);
}
$old=STInput::get('room_num_search');


if(!isset($field_size)) $field_size='lg';
?>
<div class="form-group form-group-<?php echo esc_attr($field_size)?>">
    <label for="field-hotel-room-num"><?php echo esc_html($title)?></label>
    <select  class="form-control" name="room_num_search">
        <?php $adult_max=14;
        for($i=1;$i<=$adult_max;$i++){
            echo "<option ".selected($old,$i,false)." value='{$i}'>{$i}</option>";
        }
        ?>
    </select>
</div>