<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Hotel field children
 *
 * Created by ShineTheme
 *
 */
$default=array(
    'title'=>''
);

if(isset($data)){
    extract(wp_parse_args($data,$default));
}else{
    extract($default);
}
$old=STInput::get('infant_num');

if(!isset($field_size)) $field_size='lg';
?>
<div class="form-group form-group-<?php echo esc_attr($field_size)?>">
    <label for="field-hotel-infant"><?php echo esc_html( $title)?></label>
    <select id="field-hotel-infant" class="form-control" name="infant_num">
        <?php $adult_max=st()->get_option('hotel_max_child',14);
        for($i=0;$i<=$adult_max;$i++){
            echo "<option ".selected($old,$i,false)." value='{$i}'>{$i}</option>";
        }
        ?>
    </select>
    <small><?php echo __('0-4 Years', ST_TEXTDOMAIN); ?></small>
</div>