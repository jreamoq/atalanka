<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Hotel field adult
 *
 * Created by ShineTheme
 *
 */
$default=array(
    'title'=>'',

);

if(isset($data)){
    extract(wp_parse_args($data,$default));
}else{
    extract($default);
}

if(!isset($field_size)) $field_size='lg';

$old=STInput::get('adult_number');
?>
<div class="form-group form-group-<?php echo esc_attr($field_size) ?>">
    <label for="field-hotel-adult"><?php echo esc_html($title)?></label>
    <select  class="form-control" name="adult_number">
        <?php $adult_max=st()->get_option('hotel_max_adult',14);
            for($i=1;$i<=$adult_max;$i++){
                echo "<option ".selected($old,$i,false)." value='{$i}'>{$i}</option>";
            }
        ?>
    </select>
</div>