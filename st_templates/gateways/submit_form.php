<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Payment
 *
 * Created by ShineTheme
 *
 */
?>
<div class="pm-info ct-bank-info">
    <?php $desc = st()->get_option('submit_form_desc'); ?>
    <?php if(!empty($desc)){ ?>
	    <p><?php echo esc_html($desc); ?></p>
    <?php } ?>
    <?php
    $accounts = st()->get_option('submit_form_account');
    if(!empty($accounts)){
        ?>
        <table class="table-responsive">
            <tr class="hidden-xs">
                <!--<th><?php /*echo __('Account name', ST_TEXTDOMAIN); */?></th>-->
                <th><?php echo __('Account number', ST_TEXTDOMAIN); ?></th>
                <th><?php echo __('Bank name', ST_TEXTDOMAIN); ?></th>
                <!--<th><?php /*echo __('Sort code', ST_TEXTDOMAIN); */?></th>
                <th><?php /*echo __('IBAN', ST_TEXTDOMAIN); */?></th>-->
                <th><?php echo __('BIC / Swift', ST_TEXTDOMAIN); ?></th>
            </tr>
        <?php
        foreach ($accounts as $k => $v){
            ?>
            <tr>
                <!--<td><span class="hidden-lg hidden-md hidden-sm"><?php /*echo __('Account name', ST_TEXTDOMAIN); */?>: </span><?php /*echo $v['account_name']; */?></td>-->
                <td><span class="hidden-lg hidden-md hidden-sm"><?php echo __('Account number', ST_TEXTDOMAIN); ?>: </span><?php echo $v['account_number']; ?></td>
                <td><span class="hidden-lg hidden-md hidden-sm"><?php echo __('Bank name', ST_TEXTDOMAIN); ?>: </span><?php echo $v['bank_name']; ?></td>
                <!--<td><span class="hidden-lg hidden-md hidden-sm"><?php /*echo __('Sort code', ST_TEXTDOMAIN); */?>: </span><?php /*echo $v['sort_code']; */?></td>
                <td><span class="hidden-lg hidden-md hidden-sm"><?php /*echo __('IBAN', ST_TEXTDOMAIN); */?>: </span><?php /*echo $v['iban']; */?></td>-->
                <td><span class="hidden-lg hidden-md hidden-sm"><?php echo __('BIC / Swift', ST_TEXTDOMAIN); ?>: </span><?php echo $v['bic']; ?></td>
            </tr>
            <?php
        }
        ?>
        </table>
            <?php
    }
    ?>
</div>